import hvac
from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase


class LookupModule(LookupBase):
    def run(self, terms, variables=None, **kwargs):
        ret = []
        for secret in terms:
            key = None
            if ":" in secret:
                secret, key = secret.split(":", 1)
            vault = hvac.Client()
            response = vault.read("o-secret/" + secret)
            if response is None:
                raise AnsibleError("secret '%s' not found" % secret)
            if key is not None:
                ret.append(response["data"][key])
            else:
                ret.append(response["data"])
        return ret
