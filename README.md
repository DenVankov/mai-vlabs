#### Настройка окружения

```shell script
conda create -n ansible python=3.7
conda activate ansible
python3 -m pip install -r requirements.txt
```

#### Плейбуки
Чтобы раскатить плейбук можно воспользоваться командочкой. Если добавить флаг `--check` то изменения применяться не будут.
```shell script
ansible-playbook -v playbooks/gpu.yml --diff --user <username>
```

#### Линтеры

```shell script
yamllint --strict inventory playbooks roles
ansible-lint -v roles/* playbooks/*.yml
```

Про `ansible-lint` из документации:

> Its main goal is to promote proven practices, patterns and behaviors while avoiding common pitfals
> that can easily lead to bugs or make code harder to maintain.

#### Локальное тестирование c Vagrant

* Устанавливаем Vagrant: https://www.vagrantup.com

    * Документация: https://www.vagrantup.com/docs/index

* Устанавливаем VirtualBox: https://www.virtualbox.org (если сразу не установится на Mac: https://apple.stackexchange.com/a/301305)

    * Документация: https://www.virtualbox.org/manual

* Запускаем следующее:

    ```shell script
    cd vagrant
    vagrant up
    ```

    Если все прошло хорошо, то должно было запуститься 2 виртуальные машины
    на которых уже раскатаны некоторые common роли с ip '192.168.10.1{1..2}'.

    Далее можно подключиться к ним по ssh:
    ```shell script
    ssh 192.168.10.11
    ```

    Попробовать раскатить что-то через ansible:
    ```shell script
    cd provisioning
    ansible-playbook default.yml
    ```

* Когда закончили работу:

    ```shell script
    cd vagrant
    vagrant destroy --force
    ```

---

### LIBRARY

Плагины для Ansible, например, для получения секретов из Vault.

### PLAYBOOKS

* **playbooks/gpu_lvm.yml**

    Настройка дисков и logical volume для GPU.

* **playbooks/gpu.yml**

    Настройка GPU.

* **playbooks/rnd_lvm.yml**

    Настройка дисков и logical volume для RND.

* **playbooks/rnd.yml**

    Настройка RND.
